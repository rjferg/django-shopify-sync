from __future__ import unicode_literals

import shopify
from django.db import models
from jsonfield import JSONField

from ..encoders import ShopifyDjangoJSONEncoder
from .base import ShopifyDatedResourceModel


class Theme(ShopifyDatedResourceModel):
    shopify_resource_class = shopify.resources.Theme

    name = models.CharField(max_length=64)
    previewable = models.BooleanField(default=False)
    processing = models.BooleanField(default=False)
    role = models.CharField(max_length=64)
    theme_store_id = models.CharField(max_length=20, null=True, blank=True)

    class Meta:
        app_label = 'shopify_sync'

